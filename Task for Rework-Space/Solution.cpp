﻿#include <iostream>
using namespace std;

class Coordinates {

    int x;
    int y;

public:

    int getX() { return x; }
    int getY() { return y; }

    void setX(int x) { this->x = x; }
    void setY(int y) { this->y = y; }

};

int main()
{
    int width; // the number of cells on the X axis
    cin >> width; cin.ignore();
    int height; // the number of cells on the Y axis
    cin >> height; cin.ignore();

    char grid[height][width];

    for (int i = 0; i < height; i++) {
        string line; // width characters, each either 0 or .
        getline(cin, line);

        for (int j = 0; j < width; j++) {
            grid[i][j] = line[j];
        }
    }
    Coordinates present;
    Coordinates right;
    Coordinates bottom;
    for (int i = 0; i < height; i++) {
        for (int j = 0; j < width; j++) {
            if (grid[i][j] == '0') {
                present.setX(j);
                present.setY(i);

                int j_ = j, i_ = i;
                while ((j_++) < width && grid[i][j_] == '.');
                if (j_ < width) {
                    right.setX(j_);
                    right.setY(i);
                }
                else {
                    right.setX(-1);
                    right.setY(-1);
                }


                while ((i_++) < height && grid[i_][j] == '.');
                if (i_ < height) {
                    bottom.setX(j);
                    bottom.setY(i_);
                }
                else {
                    bottom.setX(-1);
                    bottom.setY(-1);
                }


                cout << present.getX() << " " << present.getY() << " " << right.getX() << " " << right.getY() << " " << bottom.getX() << " " << bottom.getY() << endl;

            }
        }
    }
}